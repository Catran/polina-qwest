import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/start",
    name: "Start",
    component: () =>
      import(/* webpackChunkName: "start" */ "../views/Start.vue")
  },
  {
    path: "/task",
    name: "Task",
    component: () => import(/* webpackChunkName: "task" */ "../views/Task.vue"),
    children: [
      {
        path: ":id",
        component: () =>
          import(/* webpackChunkName: "TaskNumber" */ "../views/TaskNumber.vue")
      }
    ]
  },
  {
    path: "/finish",
    name: "Finish",
    component: () =>
      import(/* webpackChunkName: "start" */ "../views/Finish.vue")
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
